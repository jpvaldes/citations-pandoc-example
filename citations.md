---
title:     Simple example of citations in markdown with Pandoc
subtitle:  And it really works.
author:    JPVH
date:      October 2018
version:   0.2
---

# Using markdown and pandoc with citations {#sec:section_one}

The pandoc documentation [@pandocweb] has some examples on how to use pandoc to
include citations in the output document. Pandoc uses a filter based on citeproc
[@citeprochs] that does the magic.

It looks like it is also possible to insert notes, like this one[^1].

# Cross references {#sec:section_two}

There is also a [pandoc-crossref][pandoc-crossref-docs][@citecrossref] filter to
create cross-references in Pandoc's markdown documents.

This should cross reference [@sec:section_one] and this very section:
[@sec:section_two].

# A thesis template in markdown

A [project][md-thesis] showing a template for a thesis written in markdown with output to
HTML, docx, and PDF formats.

# References

[^1]: A *nice* **note**.

[pandoc-crossref-docs]: https://lierdakil.github.io/pandoc-crossref/ "Pandoc Crossref filter"
[md-thesis]: https://github.com/tompollard/phd_thesis_markdown "Template thesis in markdown"
