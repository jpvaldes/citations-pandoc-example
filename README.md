This is just a simple example to have as reference of how to use Markdown with
bibliographical citations.

Just run `make all` to produce a `.pdf` and a `.html`.

Dependencies: Pandoc, LaTeX, and make.