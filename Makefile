
TITLE := citations

all: $(TITLE).pdf $(TITLE).html

$(TITLE).pdf: $(TITLE).md $(TITLE).bib
	pandoc $(TITLE).md \
		-s \
		--bibliography $(TITLE).bib \
		--filter pandoc-crossref \
		--filter pandoc-citeproc \
		-V fontsize=12pt \
		-V papersize=a4paper \
		-V documentclass=report \
		-V colorlinks=true \
		-V mainfont="Gentium Book Basic" \
		--pdf-engine=xelatex \
		-o $(TITLE).pdf

$(TITLE).html: $(TITLE).md $(TITLE).bib
	pandoc $(TITLE).md \
		-s \
		--bibliography $(TITLE).bib \
		--filter pandoc-crossref \
		--filter pandoc-citeproc \
		-o $(TITLE).html

ieee: $(TITLE).md $(TITLE).bib ieee.csl
	pandoc $(TITLE).md \
		-s \
		--bibliography $(TITLE).bib \
		--filter pandoc-crossref \
		--filter pandoc-citeproc \
		--csl ieee.csl \
		-V fontsize=12pt \
		-V papersize=a4paper \
		-V documentclass=report \
		-V colorlinks=true \
		-V mainfont="Gentium Book Basic" \
		--pdf-engine=xelatex \
		-o $(TITLE).pdf

nature: $(TITLE).md $(TITLE).bib nature.csl
	pandoc $(TITLE).md \
		-s \
		--bibliography $(TITLE).bib \
		--number-sections \
		--filter pandoc-crossref \
		--filter pandoc-citeproc \
		--csl nature.csl \
		-V fontsize=12pt \
		-V papersize=a4paper \
		-V documentclass=report \
		-V colorlinks=true \
		-V mainfont="Gentium Book Basic" \
		--pdf-engine=xelatex \
		-o $(TITLE).pdf

clean-all:
	rm -f $(TITLE).pdf $(TITLE).html
